﻿//ITERATING FILE N TIMES WHERE N IS AMOUNT OF ELEMENTS IN PARSELIST MUST BE CHECKED CASE OF O(n^2)) 
var fs = require('fs'),  stats = require("stats-lite");
var parseLst = ["count_pending_messages" , "get_messages" , "get_friends_progress" , "get_friends_score" , "POST path=/api/users/", "GET path=/api/users/"];
var _stats;
fs.readFile(Logger.Getlog(), 'utf8', function (err, data) {
    console.log("--------state: getting data wait please-------");
    var data = data.split('heroku[router]:'); 
    for (  _elem in parseLst) {
        _stats = new Stats();
        for (row in data) { 
            if (data[row].indexOf(parseLst[_elem]) != -1) {
                _stats.responseArr.push(data[row]);
                _stats.dynoArr.push(data[row].split('dyno=')[1].split(' ')[0])
                connectTime = parseFloat(data[row].split('connect=')[1].split('ms')[0]);
                serviceTime = parseFloat(data[row].split('service=')[1].split('ms')[0]);
                _stats.responseCtArr.push(connectTime + serviceTime);

            }
        }
        
        console.log(parseLst[_elem] + " the median is  " + stats.median(_stats.responseCtArr));
        console.log(parseLst[_elem] + " complete response time is  " + _stats.responseArr.length);
        console.log(parseLst[_elem] + " mean one was  " + stats.mean(_stats.responseCtArr));
        console.log(parseLst[_elem] + " most seen dyno  was " + modeWrap(_stats.dynoArr));
        console.log(parseLst[_elem] + " mode was " + stats.mode(_stats.responseCtArr));
   
    }
    console.log("-----------state: end of data parsing.--------- ");
})
function modeWrap(arr) {
    var len = arr.length
    if (len == 0) return null;
    var maxElem = arr[0], modeInst = {}, maxCt = 1;  
    for (var i = 0; i < len; i++) {
        var ele = arr[i]; 
        if (modeInst[ele] == null)
            modeInst[ele] = 1;
        else
            modeInst[ele]++;
        
        if (modeInst[ele] > maxCt) {
            maxElem = ele;
            maxCt = modeInst[ele];
        }
        else if (modeInst[ele] == maxCt) {
            maxElem += '&' + ele;
            maxCt = modeInst[ele];
        }
    }
    return maxElem;
}

var Class = require('classjs');
var Stats = Class.define({
    properties : {
        responseArr : "Array",
        responseCtArr : "Array",
        dynoArr : "Array",
    }
    ,
    constructor : function () {
        this.responseArr = [];
        this.responseCtArr = [];
        this.dynoArr = [];
         
    }
});